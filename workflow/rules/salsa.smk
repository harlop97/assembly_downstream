samplesheet = pd.read_table(config["samples"], dtype=str)

rule bam_to_bed:
    input:
        filt_bio
    output:
        temporary(os.path.join(config["intermed"], "salsa_scaffolding/bed_files/{sample}.bed"))
    conda:
        "../envs/salsa1.yaml"
    log:
        os.path.join(config["logs"], "salsa_scaffolding/bam_to_bed/{sample}.log")
    benchmark:
        os.path.join(config["benchmark"], "salsa_scaffolding/bam_to_bed/{sample}.txt")
    threads:
        24
    shell:
        "bedtools bamtobed -i {input} > {output} 2> {log}"

rule sort_bed:
    input:
        os.path.join(config["intermed"], "salsa_scaffolding/bed_files/{sample}.bed")
    output: 
        temporary(os.path.join(config["intermed"], "salsa_scaffolding/sorted_bed/{sample}.bed"))
    conda:
        "../envs/salsa1.yaml"
    log:
        os.path.join(config["logs"], "salsa_scaffolding/sort_bed/{sample}.log")
    benchmark:
        os.path.join(config["benchmark"], "salsa_scaffolding/sort_bed/{sample}.txt")
    threads:
        24
    shell:
        "sort -k 4 {input} > {output} 2> {log}" 

rule salsa_scaffolding:
    input:
        assembly = os.path.join(config["results"], "Pac_bionano/hybridScaffold.fasta"),
        lengths = os.path.join(config["results"], "Pac_bionano/hybridScaffold.fasta.fai"),
        alignment = expand(os.path.join(config["intermed"], "salsa_scaffolding/sorted_bed/{sample}.bed"), zip, sample=samplesheet["sample"])
    output:
        os.path.join(config["results"], "salsa_scaffolds/scaffolds_FINAL.fasta")
    params:
        params = "-e {hiC_enzymes} -i {iter} {clean} {print} {dupl} {exp}".format(**config['salsa']),
        outdir = os.path.join(config["results"], "salsa_scaffolds")
    conda:
        "../envs/salsa2.yaml"
    log:
        os.path.join(config["logs"], "salsa_scaffolding/scaffolding.log")  
    benchmark:
        os.path.join(config["benchmark"], "salsa_scaffolding/scaffolding.txt")
    threads:
        24
    shell:
        "run_pipeline.py -a {input.assembly} -l {input.lengths} -b {input.alignment} {params.params} -o {params.outdir} 2> {log}"
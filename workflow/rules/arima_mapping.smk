#--------------------Input Functions --------------------

def tech_input(wildcards):
    if len(samplesheet[(samplesheet["sample"] == wildcards.sample) & (samplesheet["unit_bio"] == wildcards.unit_bio)]) != 1:
        return (expand(os.path.join(config["intermed"], "arima_mapping/paired_read_groups/{{sample}}_{{unit_bio}}_{unit_tech}.bam"), unit_tech = list(range(1,len(samples[(samples["sample"] == wildcards.sample) & (samples["unit_bio"] == wildcards.unit_bio)])+1))))

def filt_tech(wildcards):
    if len(samplesheet[(samplesheet["sample"] == wildcards.sample) & (samplesheet["unit_bio"] == wildcards.unit_bio)]) == 1:
        return os.path.join(config["intermed"], "arima_mapping/paired_read_groups/{sample}_{unit_bio}_1.bam")
    else:
        return os.path.join(config["intermed"], "arima_mapping/merged_tech/{sample}_{unit_bio}_1.bam")

def bio_input(wildcards):
    if len(samplesheet[(samplesheet["sample"] == wildcards.sample) & (samplesheet["unit_tech"] == "1")]) != 1:
        return (expand(os.path.join(config["intermed"], "arima_mapping/final/{{sample}}_{unit_bio}.bam"), unit_bio = list(range(1,len(samples[(samples["sample"] == wildcards.sample) & (samples["unit_tech"] == "1")])+1))))

def filt_bio(wildcards):
    if len(samplesheet[(samplesheet["sample"] == wildcards.sample) & (samplesheet["unit_tech"] == "1")]) == 1:
        return os.path.join(config["intermed"], "arima_mapping/final/{sample}_1.bam")
    else:
        return os.path.join(config["intermed"], "arima_mapping/merge_bio_repl/{sample}.bam")

def get_hybrid_scaffold(wildcards):
    checkpoint_output = checkpoints.hybrid_scaffolding.get(**wildcards).output[0]
    return expand(os.path.join(config["results"], "bionano/hybrid_scaffolds/{i}_HYBRID_SCAFFOLD.fasta"),
        i = glob_wildcards(os.path.join(checkpoint_output, "{i}_HYBRID_SCAFFOLD.fasta")).i)

#-------------------- Workflow --------------------
rule rename_bionano_file:
    input:
        get_hybrid_scaffold
    output:
        os.path.join(config["results"], "bionano/hybridScaffold.fasta")
    log:
        os.path.join(config["logs"], "arima_mapping/rename_bionano.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/rename_bionano.txt")
    shell: 
        "mv {input} {output} 2> {log}"

rule bwa_indexing:
    input:
        os.path.join(config["results"], "bionano/hybridScaffold.fasta")
    output:
        output1 = os.path.join(config["results"], "bionano/hybridScaffold.fasta.amb"),
        output2 = os.path.join(config["results"], "bionano/hybridScaffold.fasta.ann"),
        output3 = os.path.join(config["results"], "bionano/hybridScaffold.fasta.bwt"),
        output4 = os.path.join(config["results"], "bionano/hybridScaffold.fasta.pac"),
        output5 = os.path.join(config["results"], "bionano/hybridScaffold.fasta.sa")
    params:
        algorithm = config["bwa_algorithm"],
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"],"arima_mapping/bwa_indexing.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/bwa_indexing.txt")
    threads:
        24
    shell:
        "bwa index -a {params.algorithm} {input} 2> {log}"

rule r1_mapping:
    input:
        read1 = lambda wc: samplesheet[(samplesheet["sample"] == wc.sample) & (samplesheet["unit_bio"] == wc.unit_bio) & (samplesheet["unit_tech"] == wc.unit_tech)].fq1,
        ref = os.path.join(config["results"], "bionano/hybridScaffold.fasta"),
        linker = os.path.join(config["results"], "bionano/hybridScaffold.fasta.sa")
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/unprocessed_bam/{sample}_{unit_bio}_{unit_tech}_R1.bam"))
    params:
        config["bwa_mismatch_pen"]
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/mapping/{sample}_{unit_bio}_{unit_tech}_R1.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/mapping/{sample}_{unit_bio}_{unit_tech}_R1.txt")
    threads:
        workflow.cores * 0.5
    shell:
        "bwa mem -t {threads} {input.ref} {input.read1} -B {params} | samtools view --threads {threads} -h -b -o {output} 2> {log}"

rule r2_mapping:
    input:
        read2 = lambda wildcards: samplesheet[(samplesheet["sample"] == wildcards.sample) & (samplesheet["unit_bio"] == wildcards.unit_bio) & (samplesheet["unit_tech"] == wildcards.unit_tech)].fq2,
        ref = os.path.join(config["results"], "bionano/hybridScaffold.fasta"),
        linker = os.path.join(config["results"], "bionano/hybridScaffold.fasta.sa")
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/unprocessed_bam/{sample}_{unit_bio}_{unit_tech}_R2.bam"))
    params:
        config["bwa_mismatch_pen"]
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/mapping/{sample}_{unit_bio}_{unit_tech}_R2.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/mapping/{sample}_{unit_bio}_{unit_tech}_R2.txt")
    threads:
        workflow.cores * 0.5
    shell:
        "bwa mem -t {threads} {input.ref} {input.read2} -B {params} | samtools view --threads {threads} -h -b -o {output} 2> {log}"

rule r1_filter5end:
    input:
        os.path.join(config["intermed"], "arima_mapping/unprocessed_bam/{sample}_{unit_bio}_{unit_tech}_R1.bam")
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/filtered_bam/{sample}_{unit_bio}_{unit_tech}_R1.bam"))
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/filter5end/{sample}_{unit_bio}_{unit_tech}_R1.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/filter5end/{sample}_{unit_bio}_{unit_tech}_R1.txt")
    threads:
        workflow.cores * 0.45
    shell:
        "samtools view -h {input} | perl scripts/filter_five_end.pl | samtools view -Sb -o {output} 2> {log}"

rule r2_filter5end:
    input:
        os.path.join(config["intermed"], "arima_mapping/unprocessed_bam/{sample}_{unit_bio}_{unit_tech}_R2.bam")
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/filtered_bam/{sample}_{unit_bio}_{unit_tech}_R2.bam"))
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/filter5end/{sample}_{unit_bio}_{unit_tech}_R2.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/filter5end/{sample}_{unit_bio}_{unit_tech}_R2.txt")
    threads:
        workflow.cores * 0.45
    shell:
        "samtools view -h {input} | perl scripts/filter_five_end.pl | samtools view -Sb -o {output} 2> {log}"

rule samtools_faidx:
    input:
        linker = os.path.join(config["results"], "bionano/hybridScaffold.fasta.sa"),
        reference = os.path.join(config["results"], "bionano/hybridScaffold.fasta"),
    output:
        os.path.join(config["results"], "bionano/hybridScaffold.fasta.fai"),
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/samtools_faidx.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/samtools_faidx.txt")
    threads:
        workflow.cores * 0.1
    shell:
        "samtools faidx {input.reference} -o {output} 2> {log}"

rule pair_reads:
    input:
        read1 = os.path.join(config["intermed"], "arima_mapping/filtered_bam/{sample}_{unit_bio}_{unit_tech}_R1.bam"),
        read2 = os.path.join(config["intermed"], "arima_mapping/filtered_bam/{sample}_{unit_bio}_{unit_tech}_R2.bam"),
        faidx = os.path.join(config["results"], "bionano/hybridScaffold.fasta.fai"),
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/paired/{sample}_{unit_bio}_{unit_tech}.bam"))
    params:
        config["mapq_filter"]
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/paired_reads/{sample}_{unit_bio}_{unit_tech}.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/paired_reads/{sample}_{unit_bio}_{unit_tech}.txt")
    threads:
        24
    shell:
        "perl scripts/two_read_bam_combiner.pl {input.read1} {input.read2} samtools {params} | samtools view -bS -t {input.faidx} - | samtools sort --threads {threads} -o {output} 2> {log}"

rule add_read_groups:
    input:
        os.path.join(config["intermed"], "arima_mapping/paired/{sample}_{unit_bio}_{unit_tech}.bam")
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/paired_read_groups/{sample}_{unit_bio}_{unit_tech}.bam"))
    params:
        sampleName = "{sample}",
        library = "{sample}",
        other_p = "PU={platform_unit} PL={platform}".format(**config['add_read_groups']) 
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/paired_read_groups/{sample}_{unit_bio}_{unit_tech}.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/paired_read_groups/{sample}_{unit_bio}_{unit_tech}.txt")
    threads:
        24
    shell:
        "picard AddOrReplaceReadGroups I={input} O={output} SM={params.sampleName} LB={params.library} {params.other_p} 2> {log}"

rule merge_tech_repl:
    input:
        tech_input
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/merged_tech/{sample}_{unit_bio}_1.bam"))
    params:
        "--ASSUME_SORTED {sorted} --USE_THREADING {threading} --VALIDATION_STRINGENCY {stringency}".format(**config['merge_repl'])
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/merged_tech_repl/{sample}_{unit_bio}_1.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/merged_tech_repl/{sample}_{unit_bio}_1.txt")
    threads:
        2 
    shell:
      "picard MergeSamFiles -I {input} -O {output} {params} 2> {log}"

rule mark_duplicates:
    input:
        filt_tech
    output:
        bam = temporary(os.path.join(config["intermed"], "arima_mapping/final/{sample}_{unit_bio}.bam")),
        metric = os.path.join(config["intermed"], "arima_mapping/final/metric_{sample}_{unit_bio}.txt")
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/mark_duplicates/{sample}_{unit_bio}.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/mark_duplicates/{sample}_{unit_bio}.txt")
    threads:
        24
    shell:
        "picard MarkDuplicates -Xmx20g I={input} O={output.bam} M={output.metric} 2> {log}"

rule merge_bio_repl:
    input:
        bio_input
    output:
        temporary(os.path.join(config["intermed"], "arima_mapping/merge_bio_repl/{sample}.bam"))
    params:
        params = "-- ASSUME_SORTED {sorted} --USE_THREADING {threading} --VALIDATION_STRINGENCY {stringency}".format(**config['merge_repl'])
    conda:
        "../envs/arima_mapping.yaml"
    log:
        os.path.join(config["logs"], "arima_mapping/merged_bio_repl/{sample}.log")
    benchmark:
        os.path.join(config["benchmark"], "arima_mapping/merged_bio_repl/{sample}.txt")
    threads:
        2 
    shell:
        "picard MergeSamFiles -I {input} -O {output} {params.params} 2> {log}"


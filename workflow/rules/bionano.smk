rule setup_pybionano:
    output:
        touch("pybionano_installed")
    conda:
        "../envs/bionano.yaml"
    log:
        config["logs"] + "bionano/setup_pybionano.log"
    shell:
        "cd scripts/bionano/tools/pipeline/1.0/bionano_packages/pyBionano && python3 setup.py install"

#rule bnx_to_cmap:
 #   input:
  #      linker = "pybionano_installed",
   #     bnx = config["bnx"],
    #    cmap = config["cmap"]
    #output:
     #   touch("bnx")
    #params:
     #   outdir = config["intermed"] + "bionano_cmap_assembly",
      #  script = config["refAligner2"],
       # xml = config["bionano"] + "tools/pipeline/1.0/RefAligner/1.0/optArguments_nonhaplotype_DLE1_saphyr.xml"
    #conda:
     #   "../envs/bionano.yaml"
    #log:
    #    config["logs"] + "bionano/bnx_cmap.log"
    #threads:
     #   24
    #shell:
        #"python3 scripts/bionano/tools/pipeline/1.0/Pipeline/1.0/pipelineCL.py -h"
     #   "python3 scripts/bionano/tools/pipeline/1.0/Pipeline/1.0/pipelineCL.py -b {input.bnx} --species-reference non_human -j {threads} -i 5 -l {params.outdir} -t {params.script} -a {params.xml}"

rule setup_perl:
    output:
        touch("perl_packages_installed")
    conda:
        "../envs/bionano.yaml"
    log:
        os.path.join(config["logs"], "bionano/setup_perl.log")
    benchmark:
        os.path.join(config["benchmark"], "bionano/setup_perl.txt")
    shell:
        "cpanm --force PerlIO::Util Config::Simple File::Slurp File::Copy::Recursive XML::Simple DateTime DateTime::Format::Human::Duration List::MoreUtils 2> {log}"

checkpoint hybrid_scaffolding:
    input: 
        linker = "perl_packages_installed",
        seq = config["initial"],
        cmap = config["cmap"]
    output:
        directory(os.path.join(config["results"], "bionano/hybrid_scaffolds"))
    conda:
        "../envs/bionano.yaml"
    params:
        script = os.path.join(config["bionano"], "tools/pipeline/1.0/HybridScaffold/1.0/hybridScaffold.pl"),
        RefAligner = os.path.join(config["bionano"], "tools/pipeline/1.0/RefAligner/1.0/RefAligner"),
        enzyme_xml = os.path.join(config["bionano"], config["enzyme_xml"]),
        params = "-u {bionano_enzyme} -B {conM} -N {conS} {trimm}".format(**config['hybrid']),
        outdir = os.path.join(config["results"], "bionano")
    log:
        os.path.join(config["logs"], "bionano/hybrid_scaffolding.log")
    benchmark:
        os.path.join(config["benchmark"], "bionano/hybrid_scaffolding.txt")
    threads:
        24
    shell:
        "perl {params.script} -r {params.RefAligner} -n {input.seq} -b {input.cmap} -o {params.outdir} {params.params} -c {params.enzyme_xml} -f 2> {log}"

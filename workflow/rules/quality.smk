rule quast:
    input:
        assembly = os.path.join(config["results"], "salsa_scaffolds/scaffolds_FINAL.fasta")
    output:
        os.path.join(config["results"], "quality/quast/report.pdf")
    params:
        params = "{cell_type} {genome_size} {k_mer_stats} {reference} {fragmented}".format(**config['quast_params']),
        out_dir = os.path.join(config["results"], "quality/quast")
    threads:
        workflow.cores - 1
    conda:
        "../envs/quality.yaml"
    log:
        os.path.join(config["logs"], "quality/quast.log")
    benchmark:
        os.path.join(config["benchmark"], "quality/quast.txt")
    shell:
        "quast {input.assembly} -o {params.out_dir} --threads {threads} {params.params} 2> {log}"
 
rule download_busco_db:
    output:
        touch("busco_db_downloaded")
    params:
        params = "--download_path {db_path} --download {db} {quiet}".format(**config['busco_db_params']),
    threads:
        1
    conda:
        "../envs/quality.yaml"
    log:
        os.path.join(config["logs"], "quality/download_busco_db.log")
    benchmark:
        os.path.join(config["benchmark"], "quality/download_busco_db.txt")
    shell:
        "busco {params.params} 2> {log}"

rule busco:
    input:
        linker = "busco_db_downloaded",
        assembly = os.path.join(config["results"], "salsa_scaffolds/scaffolds_FINAL.fasta")
    output:
        os.path.join(config["results"], "quality/", config["busco_run_name"], "/run_", config["busco_db_params"]["db"], "/short_summary.txt")
    params:
        params = "-m {mode} {overwrite}".format(**config['busco_params']),
        run_name = config["busco_run_name"],
        outdir = os.path.join(config["results"], "quality/"),
        db = os.path.join(config["busco_db_params"]["db_path"], "lineages/", config["busco_db_params"]["db"])
    threads:
        workflow.cores
    conda:
        "../envs/quality.yaml"
    log:
        os.path.join(config["logs"], "quality/busco.log")
    benchmark:
        os.path.join(config["benchmark"], "quality/busco.txt")
    shell:
        "busco -i {input.assembly} -l {params.db} -o {params.run_name} --out_path {params.outdir} {params.params} -c {threads} 2> {log}"
 